﻿using System.Windows;
using WpfPasswordControlLibrary;

namespace Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            windowPass = new WindowPass();
            windowPass.Resources = Resources;
            Resources.Add("lablePassword", "Test!!!!!");
            windowPass.SetResourceReference(WindowPass.LablePasswordProperty, "lablePassword");
            //windowPass.SetResourceReference(WindowPass.LablePasswordProperty, "lablePassword");
            windowPass.Activated += WindowPass_Initialized;
            windowPass.ValidPassword = "123456";
            windowPass.OnClosePasswordBox += WindowPass_OnClosePasswordBox;
            windowPass.OnEnterInvalidPassword += WindowPass_OnEnterInvalidPassword;
            windowPass.OnEnterValidPassword += WindowPass_OnEnterValidPassword;
        }

        private void WindowPass_Initialized(object sender, System.EventArgs e)
        {
           // windowPass.Owner = this;
        }

        private void WindowPass_OnEnterValidPassword(object sender, System.EventArgs e)
        {
            MessageBox.Show("Correct password!!!");
        }

        private void WindowPass_OnEnterInvalidPassword(object sender, System.EventArgs e)
        {
            MessageBox.Show("Incorrect password! Maybe you will try again?");            
        }

        private void WindowPass_OnClosePasswordBox(object sender, System.EventArgs e)
        {
        }

        readonly WindowPass windowPass;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            windowPass.ShowDialog();
            
        }
    }
}

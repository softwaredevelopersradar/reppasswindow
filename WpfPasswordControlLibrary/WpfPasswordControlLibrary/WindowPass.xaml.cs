﻿using System;
using System.Windows;
using System.Windows.Input;

namespace WpfPasswordControlLibrary
{
    /// <summary>
    /// Interaction logic for WindowPass.xaml
    /// </summary>
    public partial class WindowPass : Window
    {
        public WindowPass()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            Topmost = true;
        }
        public event EventHandler OnEnterValidPassword = (sender, obj) => { };
        public event EventHandler OnEnterInvalidPassword = (sender, obj) => { };
        public event EventHandler OnClosePasswordBox = (sender, obj) => { };

        public string ValidPassword { get; set; } = "1111";

        public static readonly DependencyProperty LablePasswordProperty = DependencyProperty.Register("LablePassword", typeof(string), typeof(WindowPass), new PropertyMetadata("Enter admin password:"));

        public string LablePassword
        {
            get { return (string)GetValue(LablePasswordProperty); }
            set { SetValue(LablePasswordProperty, value); }
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            ApplyPassword();
        }

        private void NotApplyClick(object sender, RoutedEventArgs e)
        {
            NotApplyPassword();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            this.DragMove();
        }

        private void ApplyPassword()
        {
            if (string.IsNullOrEmpty(ValidPassword))
            {
                OnEnterValidPassword(this, null);
                Hide();
                return;
            }

            if (ValidPassword.Equals(PassBox.Password))
            {
                OnEnterValidPassword(this, null);
                PassBox.Password = "";
                Hide();
            }
            else
            {
                OnEnterInvalidPassword(this, null);
                PassBox.Password = "";
            }
        }

        private void NotApplyPassword()
        {
            OnClosePasswordBox(this, null);
            PassBox.Password = "";
            Hide();
        }

        private void _this_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                ApplyPassword();
            if (e.Key == Key.Escape)
                NotApplyPassword();
        }
    }
}
